package com.example.saw.phpfile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Getting student info...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    public void getUserInfo(View view) {
        GetUserInfoController controller = new GetUserInfoController(mOnGetResponseListener);
        controller.execute();
    }


    private GetResponseListener mOnGetResponseListener = new GetResponseListener() {
        @Override
        public void onStarted() {
            dialog.show();
        }

        @Override
        public void onFinished(String response) {
            dialog.dismiss();

            try {
                JSONObject jsonObjectResponse = new JSONObject(response);
                boolean isSuccess = jsonObjectResponse.getBoolean("success");
                Log.e(MainActivity.class.getSimpleName(), "is success? " + isSuccess);

                JSONArray jsonArrayData = jsonObjectResponse.getJSONArray("data");

                for (int i = 0; i < jsonArrayData.length(); i++) {
                    JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);

                    String name = jsonObjectData.getString("name");
                    Log.e(MainActivity.class.getSimpleName(), "name from server: " + name);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}
