package com.example.saw.phpfile;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by saw on 1/9/2018.
 */

public class GetUserInfoController extends AsyncTask<Void, Void, String> {
    private GetResponseListener listener;

    public GetUserInfoController(GetResponseListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onStarted();
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            URL urlToConnect = new URL("http://192.168.123.13/student.com/getStudentInfo.php");
            HttpURLConnection connection = (HttpURLConnection) urlToConnect.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();
            InputStream inputStream = connection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            return response.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String response) {
        Log.e("GetUserInfoController", "response is: " + response);
        listener.onFinished(response);
    }
}
