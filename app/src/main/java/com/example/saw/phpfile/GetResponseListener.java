package com.example.saw.phpfile;

/**
 * Created by saw on 1/9/2018.
 */

public interface GetResponseListener {
    void onStarted();
    void onFinished(String response);
}
